<?php
return [
    'user' => [
        'role' => [
            '1' => "admin", //system administrator
            '2' => "manager", //store manager
        ]
    ],
];
