<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Store;

class HomeController extends Controller
{
    public function index(){

        $users = User::where('status',1)->count();
        $stores = Store::count();

        return view('admin.index')->with(compact('users','stores'));
    }

   
}
