<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\UserProfile;
use App\Models\Store;

use Auth;
use Str;

class StoreController extends Controller
{
  
    public function index(){
        return view('admin.store.index');
    }

    public function getStoresData(Request $request){
        $columns = array_column($request->input('columns'), 'data');

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $mainDataQuery = Store::selectRaw("stores.id as DT_RowId, stores.*, 
            CONCAT(user_profile.firstname,' ',COALESCE(user_profile.middlename,''),' ',user_profile.lastname) as created_by, 
            user_profile.id as profile_id")
            ->join('user_profile', 'user_profile.user_id', 'stores.user_id');

        $totalData = $mainDataQuery->count();
        $totalFiltered = $totalData;

        if($request->filled('search.value'))
        {
            $search = $request->input('search.value');

            $havingQuery = '';
            $havingParam = array();

            foreach($columns as $key=> $col){
                if($request->input('columns')[$key]['searchable'] == 'false') continue;
                $havingQuery .= $col.' LIKE ? OR ';
                array_push($havingParam, "%".$search."%");
            }

            $mainDataQuery->havingRaw(rtrim($havingQuery, ' OR '), $havingParam);
            $totalFiltered = DB::table(DB::raw("({$mainDataQuery->toSql()}) as a"))->mergeBindings($mainDataQuery->getQuery())->count();

        }

        if($start > 0 && $limit > 0){
            $mainDataQuery->offset($start);
        }

        $mainData = $mainDataQuery->limit($limit)
            ->orderBy($order,$dir)
            ->get();

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $mainData
        );

        return response()->json($json_data);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:2|max:50',
            'description' => 'required',
            'location' => 'required',
            'phone_number' => 'required',
        ];

        if ($request->hasFile('logo')) {
            $rules['logo'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        $message = []; //custom messages

        $validator = \Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {
            return response()->json(['status' => false,'message' => 'Please check form errors.','errors' => $validator->errors()]);
        }

        try {
            
            // Handle file upload (if a logo was provided)
            $logoPath = null;
            if ($request->hasFile('logo')) {
                $uploadedFile = $request->file('logo');
                $originalName = $uploadedFile->getClientOriginalName();
                
                // Generate a unique filename using a timestamp
                $uniqueFilename = Str::slug(pathinfo($originalName, PATHINFO_FILENAME)) . '-' . time() . '.' . $uploadedFile->getClientOriginalExtension();

                $uploadedFile->storeAs('store_logos', $uniqueFilename, 'public');
                $logoPath = $uniqueFilename;
            }

            $storeData = [
                'name' => $request->name,
                'user_id' => Auth::id(),
                'description' => $request->description,
                'location' => $request->location,
                'phone_number' => $request->phone_number,
                'logo' => $logoPath, 
            ];

            if($request->filled('id')){
                $store = Store::find($request->id);
                
                //delete if logo already exists.
                if(!empty($store->logo) && !empty($logoPath)){
                    // Get the path to the logo file
                    $filePath = 'public/store_logos/' . $store->logo;
                    // Double Check if the logo file exists in storage
                    if (Storage::exists($filePath)) {
                        // Delete the logo file from storage
                        Storage::delete($filePath);
                    }
                }

                Store::find($request->id)->update($storeData);
            }else{
                $store = new Store($storeData);
                $store->save();
            }

            return response()->json(['status' => true, 'message' => 'Store add/update success.']);
        } catch (\Exception $e) {
            return $e;
            return response()->json(['status' => false, 'message' => 'Error Store add/update.']);
        }
    }

    public function edit(int $id)
    {
        $store = Store::find($id);
        return response()->json(['status' => true, 'data'=>$store]);
    }

    public function destroy(int $id)
    {
        $store = Store::find($id);     
        if(!empty($store->logo)){
            // Get the path to the logo file
            $filePath = 'public/store_logos/' . $store->logo;
            // Double Check if the logo file exists in storage
            if (Storage::exists($filePath)) {
                // Delete the logo file from storage
                Storage::delete($filePath);
            }
        }
        $store->delete();
        return response(['status' => true, 'message' => 'Store has been deleted!']);
    }
}
