<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\UserProfile;

use Auth;

class UserController extends Controller
{
   
    public function index()
    {
        return view('manager.users');
    }

    public function getUsersData(Request $request){
        $columns = array_column($request->input('columns'), 'data');

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $mainDataQuery = User::selectRaw("users.id as DT_RowId, users.*, 
            CONCAT(user_profile.firstname,' ',COALESCE(user_profile.middlename,''),' ',user_profile.lastname) as fullname, 
            user_profile.id as profile_id,
            user_profile.address,
            user_profile.gender,
            DATE_FORMAT(user_profile.bday,'%M %e, %Y') as bday")
            ->join('user_profile', 'user_profile.user_id', 'users.id')->where('users.id','<>', Auth::id());

        $totalData = $mainDataQuery->count();
        $totalFiltered = $totalData;

        if($request->filled('search.value'))
        {
            $search = $request->input('search.value');

            $havingQuery = '';
            $havingParam = array();

            foreach($columns as $key=> $col){
                if($request->input('columns')[$key]['searchable'] == 'false') continue;
                $havingQuery .= $col.' LIKE ? OR ';
                array_push($havingParam, "%".$search."%");
            }

            $mainDataQuery->havingRaw(rtrim($havingQuery, ' OR '), $havingParam);
            $totalFiltered = DB::table(DB::raw("({$mainDataQuery->toSql()}) as a"))->mergeBindings($mainDataQuery->getQuery())->count();

        }

        if($start > 0 && $limit > 0){
            $mainDataQuery->offset($start);
        }

        $mainData = $mainDataQuery->limit($limit)
            ->orderBy($order,$dir)
            ->get();

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $mainData
        );

        return response()->json($json_data);
    }

    //edit profile
    public function store(Request $request)
    {
        $rules = [
            'firstname' => 'required|min:2|max:50',
            'middlename' => 'max:50',
            'lastname' => 'required|min:2|max:50',
            'address' => 'required',
            'gender' => 'required|min:2|max:20',
        ];

        $message = []; //custom messages

        $validator = \Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {
            return response()->json(['status' => false,'message' => 'Please check form errors.','errors' => $validator->errors()]);
        }

        try {
            UserProfile::find($request->id)->update($request->all());
            return response()->json(['status' => true, 'message' => 'User Profile Updated.']);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => 'Error updating profile.']);
        }
        
    }
    
    //get profile data for edit
    public function edit(int $id)
    {
        $user = UserProfile::find($id);
        return response()->json(['status' => true, 'data'=>$user]);
    }

    //reset user passowrd
    public function resetPassword(Request $request)
    {
        $rules = [
            'password' => 'required|min:5|max:24|confirmed',
            'password_confirmation' => 'required|min:5|max:24',
        ];

        $message = []; //custom messages

        $validator = \Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {
            return response()->json(['status' => false,'message' => 'Please check form errors.','errors' => $validator->errors()]);
        }

        try {
            User::find($request->id)->update([
                'password' => Hash::make($request->password),
            ]);
            return response()->json(['status' => true, 'message' => 'User Password Updated.']);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => 'Error updating password.']);
        }
        
    }


    // //update user status
    // public function updateStatus(int $id, int $type)
    // {
    //     User::find($id)->update(['status' => $type]);
    //     return response()->json(['status' => true, 'message' => 'Status has been updated.']);
    // }

    // //update user type
    // public function updateType(int $id, int $type)
    // {
    //     User::find($id)->update(['type' => $type]);
    //     return response()->json(['status' => true, 'message' => 'Type has been updated.']);
    // }

    // //soft delete user
    // public function destroy(int $id)
    // {
    //     User::find($id)->delete();
    //     return response(['status' => true, 'message' => 'User has been deleted!']);
    // }
    
}
