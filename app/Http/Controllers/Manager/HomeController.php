<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Store;

class HomeController extends Controller
{
    public function index(){

        $stores = Store::where('user_id',Auth::id())->count();
        return view('manager.index')->with(compact('stores'));
    }

   
}
