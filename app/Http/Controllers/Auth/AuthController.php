<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\UserProfile;

use Validator;

class AuthController extends Controller
{   

    public function index(){
        return view('index');
    }

    public function authenticate(Request $request){

         $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('login')->with('status', 'Please input your credentials.');
        }else{
            $credentials = $request->validate([
                'email' => ['required'],
                'password' => ['required'],
            ]);
            
            $user = User::where('email',$request->email)->first();
            if($user->status==2) return redirect()->route('login')->with('status', 'User is deactivated!');

            if (Auth::attempt($credentials,$request->remember)) {
                $request->session()->regenerate();
                if (auth()->user()->userRole() == 'admin'){
                    return redirect(RouteServiceProvider::HOME_ADMIN);
                }
                elseif(auth()->user()->userRole() == 'manager'){
                    return redirect(RouteServiceProvider::HOME_MANAGER);
                }else{
                    return redirect()->route('login')->with('status', 'Invalid Account!');
                }
            }else{
                return redirect()->route('login')->with('status', 'Invalid Account!');
            }
        }
    }

    public function register(){
        return view('register');
    }

    public function registerStore(Request $request)
    {
        $rules = [
            'firstname' => 'required|min:2|max:50',
            'middlename' => 'max:50',
            'lastname' => 'required|min:2|max:50',
            'address' => 'required',
            'gender' => 'required|min:2|max:20',
            'email' => 'required|min:4|max:100|email|unique:users,email',
            'password' => 'required|min:5|max:24|confirmed',
            'password_confirmation' => 'required|min:5|max:24',
        ];

        $message = []; //custom messages

        $validator = \Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {
            return response()->json(['status' => false,'errors' => $validator->errors()]);
        }

        try {
            DB::beginTransaction();
        
            // Create a new User record
            $user = User::create([
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
        
            // Create a new UserProfile record associated with the user
            $userProfile = new UserProfile([
                'firstname' => $request->firstname,
                'middlename' => $request->middlename,
                'lastname' => $request->lastname,
                'address' => $request->address,
                'gender' => $request->gender,
            ]);
        
            $user->profile()->save($userProfile);
        
            // Commit the database transaction
            DB::commit();
    
            return response()->json(['status' => true, 'message' => 'Registration Completed.']);
        } catch (\Exception $e) {
            // Something went wrong, rollback the transaction
            DB::rollback();
            return response()->json(['status' => false, 'message' => 'Error creating user']);
        }

    }
    public function logout(Request $request)
    {
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        Session::flush();
        Auth::guard('web')->logout();
        return redirect()->route('login');
    }
}
