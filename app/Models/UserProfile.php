<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    use HasFactory;
    protected $table = 'user_profile';

    protected $fillable = [
        'user_id',
        'firstname',
        'middlename',
        'lastname',
        'address',
        'gender',
        'bday',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getFullName()
    {
        return $this->firstname.' '.$this->middlename.' '.$this->lastname;
    }
}
