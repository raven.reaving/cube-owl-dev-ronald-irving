<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UserProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('user_profile')->insert([
            [
                'user_id' => 1,
                'firstname' => 'Ronald Irving',
                'lastname' => 'Pablo',
                'address' => '123 Main St',
                'gender' => 'Male',
                'bday' => '1990-05-15',
            ],
            [
                'user_id' => 2,
                'firstname' => 'David',
                'lastname' => 'Pitman',
                'address' => '456 Elm St',
                'gender' => 'Male',
                'bday' => '1985-12-10',
            ],
        ]);
    }
}
