<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'email' => 'ronald@test.com',
                'password' => Hash::make('test'),
                'type' => 1, //administrator
            ],
            [
                'email' => 'david@test.com',
                'password' => Hash::make('test'),
                'type' => 2, //store manager
            ],
        ]);
    }
}
