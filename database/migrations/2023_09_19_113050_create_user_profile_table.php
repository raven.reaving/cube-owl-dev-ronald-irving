<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->string('firstname',50);
            $table->string('middlename',50)->nullable();
            $table->string('lastname',50)->nullable();
            $table->text('address')->nullable();
            $table->string('gender',20);
            $table->date('bday')->nullable();
            $table->timestamps();

            $table->foreign(['user_id'], 'profile_user_idx')->references(['id'])->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_profiles');
    }
};
