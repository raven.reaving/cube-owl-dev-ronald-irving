<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;


Route::get('/', [AuthController::class, 'index'])->name('login');
Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

//Authentications
Route::name('auth.')->group(function () {

    // For Login
    Route::controller(AuthController::class)->group(function () {
        Route::post('request', 'authenticate')->name('authenticate');
        Route::post('/register/store','registerStore')->name('register.store');
    });
    
    
});
