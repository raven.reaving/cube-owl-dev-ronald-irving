<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Manager\HomeController;
use App\Http\Controllers\Manager\UserController;
use App\Http\Controllers\Manager\StoreController;


/* Store Manager Routes */
Route::name('manager.')->middleware(['auth','role:manager'])->group(function () {
    Route::get('/', function(){return Redirect::to('manager/home');});
    Route::get('home', [HomeController::class, 'index'])->name('home');

    // Users Management Page
    Route::resource('users', UserController::class)->except([
        'create', 'show', 'update', 'index'
    ]);
    Route::get('users/list', [UserController::class, 'getUsersData'])->name('users.list');
    Route::post('users/reset-password', [UserController::class, 'resetPassword'])->name('users.reset_password');
    // Route::get('users/{user}/{status}/update-status', [UserController::class, 'updateStatus'])->name('users.update_status');
    // Route::get('users/{user}/{status}/update-type', [UserController::class, 'updateType'])->name('users.update_type');


    // Stores Management Page
    Route::resource('stores', StoreController::class)->except([
        'create', 'show', 'update'
    ]);
    Route::get('stores/list', [StoreController::class, 'getStoresData'])->name('stores.list');

});

?>