<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\StoreController;


/* Admin Routes */
Route::name('admin.')->middleware(['auth','role:admin'])->group(function () {
    Route::get('/', function(){return Redirect::to('admin/home');});
    Route::get('home', [HomeController::class, 'index'])->name('home');

    // Users Management Page
    Route::resource('users', UserController::class)->except([
        'create', 'show', 'update'
    ]);
    Route::get('users/list', [UserController::class, 'getUsersData'])->name('users.list');
    Route::post('users/reset-password', [UserController::class, 'resetPassword'])->name('users.reset_password');
    Route::get('users/{user}/{status}/update-status', [UserController::class, 'updateStatus'])->name('users.update_status');
    Route::get('users/{user}/{status}/update-type', [UserController::class, 'updateType'])->name('users.update_type');


    // Stores Management Page
    Route::resource('stores', StoreController::class)->except([
        'create', 'show', 'update'
    ]);
    Route::get('stores/list', [StoreController::class, 'getStoresData'])->name('stores.list');

});

?>