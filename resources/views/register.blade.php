<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">
  <title>CubeOwl - Registration</title>
  <link rel="icon" type="image/x-icon" href="{{ asset('dist/img/favicon.webp') }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">

    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">

    <!-- Lada Button -->
    <link type="text/css" href="{{ asset('plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <style>
        
    .login-box {
        min-width: 500px;
    }

    span.input-group-text.password-shower {
        cursor: pointer;
    }

    </style>
</head>
<body class="hold-transition login-page">

<div class="login-box">
  
  <div class="card card-outline card-primary">

    <div class="card-header text-center">
      <a href="javascript:;" class="h3">Registration</a>
    </div>
   
    <div class="card-body">
    
        @if(session('status'))
             <label class="text-center text-danger">{{ session('status') }}</label>
        @endif
      
      <form id="frm_register" method="POST" novalidate>
        @csrf
        
        <div class="row">

            <div class="input-group mb-3">
                <input type="text" name="firstname" class="form-control" placeholder="Firstname">
                <div class="input-group-append">
                    <div class="input-group-text">
                    <span class="fas fa-user"></span>
                    </div>
                </div>
                <div class="invalid-feedback"></div>
            </div>
            <div class="input-group mb-3">
                <input type="text" name="middlename" class="form-control" placeholder="Middlename">
                <div class="input-group-append">
                    <div class="input-group-text">
                    <span class="fas fa-user"></span>
                    </div>
                </div>
                <div class="invalid-feedback"></div>
            </div>
            <div class="input-group mb-3">
                <input type="text" name="lastname" class="form-control" placeholder="Lastname">
                <div class="input-group-append">
                    <div class="input-group-text">
                    <span class="fas fa-user"></span>
                    </div>
                </div>
                <div class="invalid-feedback"></div>
            </div>
            <div class="input-group mb-3">
                <input type="text" name="address" class="form-control" placeholder="Adddress">
                <div class="input-group-append">
                    <div class="input-group-text">
                    <span class="fas fa-map"></span>
                    </div>
                </div>
                <div class="invalid-feedback"></div>
            </div>
            <div class="input-group mb-3">
                <select name="gender" id="gender" class="form-control" id="gender">
                    <option value="">Please Select Gender</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
                <div class="invalid-feedback"></div>
            </div>
            <hr>
            <div class="input-group mb-3">
                <input type="email" name="email" class="form-control" placeholder="Email">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                  </div>
                </div>
                <div class="invalid-feedback"></div>
              </div>
              <div class="input-group mb-3">
                <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                <div class="input-group-append">
                    <span class="input-group-text password-shower" onclick=" password_visibile('eye3', 'password');"><i class="fa fa-eye-slash" id="eye3"></i></span>
                </div>
                <div class="invalid-feedback"></div>
              </div>
              <div class="input-group mb-3">
                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="Retype password">
                <div class="input-group-append">
                    <span class="input-group-text password-shower" onclick=" password_visibile('eye2', 'password_confirmation');"><i class="fa fa-eye-slash" id="eye2"></i></span>
                </div>
                <div class="invalid-feedback"></div>
              </div>

          <div class="col-12 p-0">
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
          </div>

          <div class="col-12 mt-2">
            <a href="{{ route('login') }}" class="text-center">Go to Sign in</a>
          </div>

        </div>

      </form>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->


<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- ladda -->
<script src="{{ asset('plugins/ladda/spin.min.js') }}"></script>
<script src="{{ asset('plugins/ladda/ladda.min.js') }}"></script>
<script src="{{ asset('plugins/ladda/ladda.jquery.min.js') }}"></script>

<script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>

<script>

    class custom_err{
        static clear(form_object){//call this using custom_err.clear(form_object)
            form_object.find('input, textarea, select, div, checkbox').removeClass('is-invalid');
        }


        //use this when you want to use the loop here directly WE USE THIS BY DEFAULT
        static display_all(errors=[],form_object){
            $.each(errors, function(key, value) {
                custom_err.display(form_object, key, value[0]);
            });
        }

        //use this when you're using the loop in error condition WE USE THIS WHEN WE WANT TO ALTER SOMETHING
        static display(form_object, field_name, error_message){//call this using custom_err.display(form_object, field_name, error_message)
            form_object.find('[name="'+field_name+'"]').addClass('is-invalid');
            form_object.find('[name="'+field_name+'"]').parent().find('.invalid-feedback').text(error_message);
        }
        
    }


    function password_visibile(icon, input){
        $("#"+icon).toggleClass('fa-eye fa-eye-slash');
        $("#"+input).attr('type', function(index, attr){
            return (attr == 'text')? 'password' : 'text';
        });
    }

    $('#frm_register').submit(function(e){
        e.preventDefault();

        const btn_submit=$(this).find('button[type="submit"]').ladda();
        btn_submit.ladda('start');

        var pdata = $(this).serialize();

        custom_err.clear($('#frm_register'));

        $.ajax({
            url: "{{ route('auth.register.store') }}",
            method: 'post',
            data: pdata,
            type: 'json',
            success: function(result) {
                if(result.status){
                    Swal.fire({
                        heightAuto: false,
                        icon: 'success',
                        title: 'Saving Success.',
                        text: result.message,
                    })
                    $('#frm_register')[0].reset();
                }else{

                    Swal.fire({
                        heightAuto: false,
                        icon: 'warning',
                        title: 'Saving Failed.',
                        text: 'Please check form errors.',
                    })

                    //show field errors
                    custom_err.display_all(result.errors,$('#frm_register'));
                }
                btn_submit.ladda('stop');
            }
        }).fail(function() {
            Swal.fire({
                heightAuto: false,
                icon: 'error',
                title: 'Database Error',
                text: 'Please Contact Web Admin.',
            })
            btn_submit.ladda('stop');
        });

    });



 


 

</script>

</body>
</html>
