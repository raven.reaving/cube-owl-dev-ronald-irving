@extends('manager.layout.app')
@section('title', 'Dashboard')
@push('styles')

<style>
    
</style>

@endpush

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $stores }}</h3>

                <p>My Stores</p>
              </div>
              <div class="icon">
                <i class="fa fa-store"></i>
              </div>
              <a href="{{route('manager.stores.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>



        </div>


        

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection

@push('page-modals')

@endpush


@push('js-scripts')
    <script>
     
        
    </script>
@endpush