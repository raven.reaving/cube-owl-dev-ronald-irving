<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Manager - @yield('title')</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('dist/img/favicon.webp') }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">

    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables-new/datatables.min.css') }}"/>

    <!-- Lada Button -->
    <link type="text/css" href="{{ asset('plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
  
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">

    <!-- GLOBAL CUSTOM STYLES -->
    <link rel="stylesheet" href="{{ asset('custom.css') }}">


    <style>
      

    </style>
    
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

    @stack('styles')    

    <!-- PAGE CSS FILES HERE -->
    

</head>
    
<body class="hold-transition sidebar-mini sidebar-collapse">

<div class="wrapper">

    <!-- Preloader -->
    <div class="preloader flex-column justify-content-center align-items-center">
      <img class="animation__wobble" src="{{ asset('dist/img/cube-owl-1.png') }}" alt="AdminLTELogo" height="60" width="60">
    </div>


    @include('manager.layout.navbar')

    @include('manager.layout.sidebar')


    <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">

        @include('manager.layout.breadcrumb')

        @yield('content')

      </div>

    <!-- /.content-wrapper -->

    @include('manager.layout.footer')

</div>

<!-- Global Modals -->

@stack('page-modals')

  <!-- Modal Edit Profile -->
  <div class="modal fade" id="modal_add_edit_profile" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="frm_add_edit_profile" method="post">
                <input name="id" type="hidden" value="">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>First Name</label>
                                <input name="firstname" type="text" class="form-control" placeholder="Enter First Name here" id="firstname">
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Middle Name</label>
                                <input name="middlename" type="text" class="form-control" placeholder="Enter Middle Name here" id="middlename">
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input name="lastname" type="text" class="form-control" placeholder="Enter Last Name here" id="lastname">
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>

                        <div class="col-md-12 mt-3">
                            <div class="form-group">
                                <label>Address</label>
                                <textarea name="address" class="form-control" placeholder="Enter Address here" rows="2" id="address"></textarea>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Gender</label>
                                <select name="gender" id="gender" class="form-control" id="gender">
                                    <option value="">Please Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Birth Date</label>
                                <input name="bday" type="date" class="form-control" id="bday">
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal reset password -->
<div class="modal fade" id="modal_reset_password" role="dialog">
  <div class="modal-dialog modal-dialog-centered modal-md">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title">Reset Password</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <form id="frm_reset_password" method="post">
              <input name="id" type="hidden" value="">
              @csrf
              <div class="modal-body">
                  <div class="row">
                    <div class="input-group mb-3">
                      <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                      <div class="input-group-append">
                          <span class="input-group-text password-shower" onclick=" password_visibile('eye3', 'password');"><i class="fa fa-eye-slash" id="eye3"></i></span>
                      </div>
                      <div class="invalid-feedback"></div>
                    </div>
                    <div class="input-group mb-3">
                      <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="Retype password">
                      <div class="input-group-append">
                          <span class="input-group-text password-shower" onclick=" password_visibile('eye2', 'password_confirmation');"><i class="fa fa-eye-slash" id="eye2"></i></span>
                      </div>
                      <div class="invalid-feedback"></div>
                    </div>      
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save</button>
              </div>
          </form>
      </div>
  </div>
</div>

<!-- End Global Modals -->

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- bs-custom-file-input -->
<script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<!-- ladda -->
<script src="{{ asset('plugins/ladda/spin.min.js') }}"></script>
<script src="{{ asset('plugins/ladda/ladda.min.js') }}"></script>
<script src="{{ asset('plugins/ladda/ladda.jquery.min.js') }}"></script>

<script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script src="{{ asset('plugins/datatables-new/datatables.min.js') }}"></script>

<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>

<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>

<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>

<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>

<script>

  class custom_err{
      static clear(form_object){//call this using custom_err.clear(form_object)
          form_object.find('input, textarea, select, div, checkbox').removeClass('is-invalid');
      }


      //use this when you want to use the loop here directly WE USE THIS BY DEFAULT
      static display_all(errors=[],form_object){
        $.each(errors, function(key, value) {
            custom_err.display(form_object, key, value[0]);
        });
      }

      //use this when you're using the loop in error condition WE USE THIS WHEN WE WANT TO ALTER SOMETHING
      static display(form_object, field_name, error_message){//call this using custom_err.display(form_object, field_name, error_message)
          form_object.find('[name="'+field_name+'"]').addClass('is-invalid');
          form_object.find('[name="'+field_name+'"]').parent().find('.invalid-feedback').text(error_message);
      }
      
  }

  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

  function swalMessage(icon,title,text){
    Swal.fire({
        heightAuto: false,
        icon: icon,
        title: title,
        text: text,
    })
  }

  function swalError(){
    Swal.fire({
        heightAuto: false,
        icon: 'error',
        title: 'Database Error',
        text: 'Please Contact Web Admin.',
    })
  }

  // global reset passwords
  function resetPassword(userId){
    custom_err.clear($('#frm_reset_password'));
    $('#frm_reset_password')[0].reset();
    $('#frm_reset_password [name="id"]').val(userId);
    $('#modal_reset_password').modal('show');
  }

  function password_visibile(icon, input){
    $("#"+icon).toggleClass('fa-eye fa-eye-slash');
    $("#"+input).attr('type', function(index, attr){
        return (attr == 'text')? 'password' : 'text';
    });
  }

  $('#frm_reset_password').submit(function(e){
      e.preventDefault();

      const btn_submit=$(this).find('button[type="submit"]').ladda();
      btn_submit.ladda('start');

      var pdata = $(this).serialize();

      custom_err.clear($('#frm_reset_password'));

      $.ajax({
          url: "{{ route('manager.users.reset_password') }}",
          method: 'post',
          data: pdata,
          type: 'json',
          success: function(result) {
              if(result.status){
                  swalMessage('success','Saving Failed.',result.message)
                  $('#frm_reset_password')[0].reset();
                  $('#modal_reset_password').modal('hide');
              }else{
                  swalMessage('warning','Saving Failed.',result.message)
                  //show field errors
                  custom_err.display_all(result.errors,$('#frm_reset_password'));
              }
              btn_submit.ladda('stop');
          }
      }).fail(function() {
          Swal.fire({
              heightAuto: false,
              icon: 'error',
              title: 'Database Error',
              text: 'Please Contact Web Admin.',
          })
          btn_submit.ladda('stop');
      });

  });


  // global profile update
  function editProfile(id){
      var url = '{{ route('manager.users.edit', ['user' => 'id']) }}';
      url = url.replace('id', id);
      $.ajax({
          url: url,
          method: 'GET',
          success: function(data) {
              $.each(data.data, function( index, value ) {
                  $('#frm_add_edit_profile [name="'+index+'"]').val(value);
              });
              $('#modal_add_edit_profile .modal-title').text('Edit Profile');
              $('#modal_add_edit_profile').modal('show');
          }
      }).fail(function() {
          swalError();
      });
  }

  //submit
  $('#frm_add_edit_profile').submit(function(e){
      e.preventDefault();

      const btn_submit=$(this).find('button[type="submit"]').ladda();
      btn_submit.ladda('start');

      var pdata = $(this).serialize();

      custom_err.clear($('#frm_add_edit_profile'));

      $.ajax({
          url: "{{ route('manager.users.store') }}",
          method: 'post',
          data: pdata,
          type: 'json',
          success: function(result) {
              if(result.status){
                  if(typeof users_table !== 'undefined'){
                    users_table.ajax.url('{{route('manager.users.list')}}').load();
                    swalMessage('success','Saving Success.',result.message);
                    $('#frm_add_edit_profile')[0].reset();
                    $('#modal_add_edit_profile').modal('hide');
                  }else{
                    location.reload();
                  }
              }else{
                  swalMessage('warning','Saving Failed.',result.message)
                  //show field errors
                  custom_err.display_all(result.errors,$('#frm_add_edit_profile'));
              }
              btn_submit.ladda('stop');
          }
      }).fail(function() {
          swalError();
          btn_submit.ladda('stop');
      });

  });

  $('#modal_add_edit_profile').on('hidden.bs.modal', function () {
      custom_err.clear($('#frm_add_edit_profile'));
      $('#frm_add_edit_profile input[name="id"]').val('');
      $('#modal_add_edit_profile .modal-title').text('');
      $('#frm_add_edit_profile')[0].reset();
  });
  


</script>

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

@stack('js-scripts') 



</body>
</html>
