@extends('manager.layout.app')
@section('title', 'My Stores')
@push('styles')

<link rel="stylesheet" href="{{ asset('plugins/ekko-lightbox/ekko-lightbox.css') }}">

@endpush

@section('content')

    <!-- Main content -->
    <section class="content">

        <div class="container-fluid">

            <div class="row">

                <div class="col-12">

                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            {{-- <h3 class="card-title">Stores List</h3> --}}
                            <button type="button" class="btn btn-info btn-sm" id="btn-create"> Create New Store </button>
                        </div>

                        <div class="card-body">

                            <div class="table-responsive">
                                <table id="table_main" class="table table-bordered table-hover"> </table>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>


@endsection

@push('page-modals')

     <!-- Modal  -->
     <div class="modal fade" id="modal_add_edit" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="frm_add_edit" method="post" enctype="multipart/form-data">
                    <input name="id" type="hidden" value="">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name="name" id="name" type="text" class="form-control" placeholder="Store Name" >
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input name="phone_number" id="phone_number" type="text" class="form-control" placeholder="Store Name" >
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" id="description" class="form-control"></textarea>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Location/Address</label>
                                    <input name="location" id="location" type="text" class="form-control" placeholder="Address">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Logo</label>
                                    <input name="logo" id="logo" type="file" class="form-control-file" accept="image/jpeg, image/png, image/jpg">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>

                            <div class="col-md-12 current_logo" style="display:none;">
                                <div class="form-group">
                                    <label>Current Logo</label>
                                </div>
                                <img src="" class="img-fuild" style="max-height: 500px;">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
      </div>


@endpush

@push('js-scripts')

    <script src="{{ asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
    <script>

        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true
            });
        });

        var table = $('#table_main').DataTable({
            order: [[ 2, "ASC" ]],
            searchDelay: 1000,
            responsive: true,
            serverSide: true,
            language: {
                search: "Search:",
                searchPlaceholder: "Store Name",
            },
            pageLength: 10,
            lengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "All"]],
            ajax: {
                "url": '{{ route("manager.stores.list") }}?new=1',
                "type": "GET",
                beforeSend: function(){
                    $('#table_main > tbody').html(
                        '<tr><td align="top" class="text-success data-tables-styles" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fa fa-circle-notch fa-spin fa-fw"></i><span> Loading...</span></td></tr>'
                    );
                },
            },
            columns: [  {
                "orderable": false,
                "searchable": false,
                "mDataProp": "id",
                "title": "Actions",
                "render": function(data, type, row, meta){

                    newdata = '<div class="btn-group">\
                                      <button type="button" class="btn btn-sm btn-info">Settings</button>\
                                      <button type="button" class="btn btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">\
                                        <span class="sr-only">Toggle Dropdown</span>\
                                      </button>\
                                      <div class="dropdown-menu" role="menu">\
                                        <a class="dropdown-item" href="javascript:;" onclick="edit('+data+')"><i class="far fa-edit"></i> Edit</a>\
                                        <a class="dropdown-item" href="javascript:;" onclick="deleteStore('+data+')"><i class="far fa-trash-alt"></i> Delete</a>\
                                      </div>\
                                    </div>'	;
                    if(type === 'display'){
                    }
                    return newdata;
                }
                },
                {
                    "mDataProp": "name",
                    "title": "Name"
                },
                {
                    "mDataProp": "description",
                    "title": "Description",
                },
                {
                    "mDataProp": "location",
                    "title": "Location",
                },
                {
                    "mDataProp": "phone_number",
                    "title": "Phone",
                },
                {
                    "mDataProp": "logo",
                    "title": "Logo",
                    "render": function(data, type, row, meta){
                        if (data !== null) {
                            var file_url = "{{ asset('storage/store_logos') }}"+"/"+ data;
                            return "<a href='"+file_url+"' data-toggle='lightbox' data-title='"+row.name+" Image' ><img src='"+file_url+"' class='' height='200' width='250'></a>";
                        }else{
                            return 'N/A';
                        }
                    }
                },
                {
                    "mDataProp": "created_by",
                    "title": "Created By",
                },
                {
                    "mDataProp": "created_at",
                    "title": "Date Created",
                }
                
            ],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'csv', title: 'Stores List',exportOptions: { columns: ':visible'}},
            ],
        });  
        

        $('#btn-create').click(function(e){
            e.preventDefault();
            $('#frm_add_edit')[0].reset();
            $('#frm_add_edit input[name="id"]').val('');
            $('#modal_add_edit .modal-title').text('Create New Store');
            $('#modal_add_edit').modal('show');
        });

        $('#frm_add_edit').submit(function(e){
            e.preventDefault();

            const btn_submit=$(this).find('button[type="submit"]').ladda();
            btn_submit.ladda('start');

            // var pdata = $(this).serialize();
            custom_err.clear($('#frm_add_edit'));

            let form = $("#frm_add_edit")[0];
            var data = new FormData(form);

            $.ajax({
                url: "{{ route('manager.stores.store') }}",
                method: 'post',
                processData: false,  // Important!
                contentType: false,
                data: data,
                type: 'json',

                success: function(result) {
                    if(result.status){

                        table.ajax.url('{{route('manager.stores.list')}}').load();
                        swalMessage('success','Saving Success.',result.message);

                        $('#frm_add_edit')[0].reset();
                        $('#modal_add_edit').modal('hide');

                    }else{

                        swalMessage('warning','Saving Failed.',result.message);
                        //show field errors
                        custom_err.display_all(result.errors,$('#frm_add_edit'));
                    }
                    btn_submit.ladda('stop');
                }
            }).fail(function() {
                swalError()
                btn_submit.ladda('stop');
            });

        });


        function edit(id){
            var url = '{{ route('manager.stores.edit', ['store' => 'id']) }}';
            url = url.replace('id', id);
            $.ajax({
                url: url,
                method: 'GET',
                success: function(data) {
                    $.each(data.data, function( index, value ) {
                        if(index=='logo'){
                           return true;
                        }
                        $('#frm_add_edit [name="'+index+'"]').val(value);
                    });

                    if (data.data.logo !== null) {
                        // Set the src attribute of the img element
                        $('.current_logo img').attr('src', '{{ asset('storage/store_logos/') }}' + '/' + data.data.logo);
                        // Show the current_logo div
                        $('.current_logo').show();
                    } else { // Hide the current_logo div if there's no logo
                        $('.current_logo').hide();
                    }
                    
                    $('#modal_add_edit .modal-title').text('Edit Store');
                    $('#modal_add_edit').modal('show');
                }
            }).fail(function() {
                swalError();
            });
        }   


        function deleteStore(id){

            var url = '{{ route('manager.stores.destroy', ['store' => 'id']) }}';
            url = url.replace('id', id);

            Swal.fire({
                title: 'Are you sure?',
                text: "Store data will be deleted.",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Delete Store.',
                closeOnConfirm: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                        }
                    });
                    $.ajax({
                        url:url,
                        method: 'DELETE',
                        success: function(result) {
                            if(result.status){
                                swalMessage('success','Delete Success.',result.message);
                                table.ajax.url('{{route('manager.stores.list')}}').load();
                            }else{
                                swalMessage('warning','Delete Failed.',result.message);
                            }
                        }
                    }).fail(function() {
                        swalError();
                    });
                }
            })

        }

    </script>

@endpush
