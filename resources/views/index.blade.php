<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">
  <title>CubeOwl - Login</title>
  <link rel="icon" type="image/x-icon" href="{{ asset('dist/img/favicon.webp') }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">

    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <style>
   
    </style>
</head>
<body class="hold-transition login-page">

<div class="login-box">
  <!-- /.login-logo -->

  <div class="login-logo">
    <img class="card-img-top" src="{{ asset('dist/img/cube-owl.png') }}" alt="CubeOwl Pte Ltd">
  </div>
  <div class="card card-outline card-primary">

    <div class="card-header text-center">
      <a href="javascript:;" class="h3">Sign in</a>
    </div>
   
    <div class="card-body">
    
        @if(session('status'))
             <label class="text-center text-danger">{{ session('status') }}</label>
        @endif
      
      <form id="login_form" method="POST" action="{{ route('auth.authenticate') }}" novalidate>
        @csrf
        <div class="input-group mb-3">
          <input type="email" class="form-control" name="email" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-portrait"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
         
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->

          <div class="col-12 mt-2">
            <a href="{{ route('register') }}" class="text-center">Create Account</a>
          </div>

        </div>

      </form>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->


<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>


</body>
</html>
