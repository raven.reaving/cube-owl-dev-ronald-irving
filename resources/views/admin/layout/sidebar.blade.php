 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-danger elevation-4">
    
    
    <a href="{{ route('admin.home') }}" class="brand-link">
      <img src="{{ asset('dist/img/cube-owl-1.png') }}" alt="COMPANY LOGO" class="brand-image img-square elevation-3">
      <span class="brand-text font-weight-bold text-white"> CubeOwl Pte Ltd </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          @if(strtolower(auth()->user()->profile->gender)=='male')
            <img src="{{ asset('dist/img/avatar5.png') }}" class="img-circle elevation-2" alt="Male Default Photo">
          @else
            <img src="{{ asset('dist/img/avatar2.png') }}" class="img-circle elevation-2" alt="Female Default Photo">
          @endif
         
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ auth()->user()->profile->getFullName()  }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column text-md nav-flat" data-widget="treeview" role="menu" data-accordion="false">
      
            <li class="nav-item">
              <a href="{{ route('admin.home') }}" class="nav-link {{ (request()->is('admin/home'))? 'active': '' }}">
                <i class="fas fa-home"></i>
                <p>Dashboard</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="{{ route('admin.users.index') }}" class="nav-link {{ (request()->is('admin/users'))? 'active': '' }}">
                <i class="fas fa-users"></i>
                <p>Users Management</p>
              </a>
            </li>

            
            <li class="nav-item">
              <a href="{{ route('admin.stores.index') }}" class="nav-link {{ (request()->is('admin/stores'))? 'active': '' }}">
                <i class="fas fa-store"></i>
                <p>Stores Management</p>
              </a>
            </li>

        

          {{-- <li class="nav-header">Menu Heading</li> --}}

         

     

         
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>