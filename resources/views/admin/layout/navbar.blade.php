
<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav mr-auto">

     
      <li class="nav-item">
        <a href="{{route('admin.users.index')}}" data-toggle="tooltip" data-html="true" data-placement="top" title="Users Management" class="btn nav-link btn-outline-success btn-block btn-sm"><i class="fa fa-users"></i>   </a>
      </li>
      &nbsp;
      <li class="nav-item">
        <a href="{{route('admin.stores.index')}}" data-toggle="tooltip" data-html="true" data-placement="top" title="Stores Management"  class="btn nav-link btn-outline-primary btn-block btn-sm"><i class="fa fa-store"></i>   </a>
      </li>
      &nbsp;
      <li class="nav-item">
        <a href="javascript:;" data-toggle="tooltip" data-html="true" data-placement="top" title="Profile Update" onclick="editProfile({{Auth::id()}})" class="btn nav-link btn-outline-info btn-block btn-sm"><i class="fa fa-user-edit"></i> </a>
      </li>
      &nbsp;
      <li class="nav-item">
        <a href="javascript:;" data-toggle="tooltip" data-html="true" data-placement="top" title="Reset Password"  onclick="resetPassword({{Auth::id()}})" class="btn nav-link btn-outline-danger btn-block btn-sm"><i class="fa fa-user-shield"></i>   </a>
      </li>


    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      

      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
        <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      

      <li class="nav-item">
        <a href="{{ route('logout') }}" class="nav-link">Logout</a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->