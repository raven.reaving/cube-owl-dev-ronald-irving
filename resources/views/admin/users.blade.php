@extends('admin.layout.app')
@section('title', 'Users Management')
@push('styles')

    <style>
  
    </style>

@endpush

@section('content')

    <!-- Main content -->
    <section class="content">

        <div class="container-fluid">

            <div class="row">

                <div class="col-12">

                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Users List</h3>
                        </div>

                        <div class="card-body">

                            <div class="table-responsive">
                                <table id="table_main" class="table table-bordered table-hover"> </table>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>


@endsection

@push('page-modals')

  


@endpush

@push('js-scripts')
    <script>

        var users_table = $('#table_main').DataTable({
            order: [[ 2, "ASC" ]],
            searchDelay: 1000,
            responsive: true,
            serverSide: true,
            language: {
                search: "Search:",
                searchPlaceholder: "User Name",
            },
            pageLength: 25,
            lengthMenu: [[25, 50, 100, 500, -1], [25, 50, 100, 500, "All"]],
            ajax: {
                "url": '{{ route("admin.users.list") }}?new=1',
                "type": "GET",
                beforeSend: function(){
                    $('#table_main > tbody').html(
                        '<tr><td align="top" class="text-success data-tables-styles" colspan="100" style="text-align: center; padding: 70px; font-size: 20px;"><i class="fa fa-circle-notch fa-spin fa-fw"></i><span> Loading...</span></td></tr>'
                    );
                },
            },
            columns: [  {
                "orderable": false,
                "searchable": false,
                "mDataProp": "id",
                "title": "Actions",
                "render": function(data, type, row, meta){

                    if(row.status==1){
                        statusButton = '<a class="dropdown-item" href="javascript:;" onclick="updateUserStatus('+data+',2)"><i class="fas fa-user-times"></i> Deactivate</a>';
                    }else{
                        statusButton = '<a class="dropdown-item" href="javascript:;" onclick="updateUserStatus('+data+',1)"><i class="fas fa-user-check"></i> Activate</a>';
                    }

                    if(row.type==1){
                        typeButton = '<a class="dropdown-item" href="javascript:;" onclick="updateUserType('+data+',2)"><i class="fas fa-house-user"></i> Assign as Store Manager</a>';
                    }else{
                        typeButton = '<a class="dropdown-item" href="javascript:;" onclick="updateUserType('+data+',1)"><i class="fas fa-user-tie"></i> Assign as Administrator</a>';
                    }

                    newdata = '<div class="btn-group">\
                                      <button type="button" class="btn btn-sm btn-info">Settings</button>\
                                      <button type="button" class="btn btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">\
                                        <span class="sr-only">Toggle Dropdown</span>\
                                      </button>\
                                      <div class="dropdown-menu" role="menu">\
                                        <a class="dropdown-item" href="javascript:;" onclick="editProfile('+row.profile_id+')"><i class="far fa-edit"></i> Edit</a>\
                                        <a class="dropdown-item" href="javascript:;" onclick="resetPassword('+data+')"><i class="fas fa-user-shield"></i> Reset Password</a>\
                                        '+statusButton+'\
                                        '+typeButton+'\
                                        <a class="dropdown-item" href="javascript:;" onclick="deleteUser('+data+')"><i class="far fa-trash-alt"></i> Delete</a>\
                                      </div>\
                                    </div>'	;
                    if(type === 'display'){
                    }
                    return newdata;
                }
            },
                {
                    "mDataProp": "fullname",
                    "title": "Fullname"
                },
                {
                    "mDataProp": "email",
                    "title": "Email",
                },
                {
                    "mDataProp": "address",
                    "title": "Address",
                },
                {
                    "mDataProp": "bday",
                    "title": "Birthday",
                },
                {
                    "mDataProp": "gender",
                    "title": "Gender",
                },
                {
                    "mDataProp": "type",
                    "title": "User Type",
                    "class": "text-center",
                    "render": function (data, type, row, meta) {
                        if (data == 1) {
                            return '<badge class="badge badge-primary">Administrator</button>';
                        }else{
                            return '<badge class="badge badge-info">Store Manager</button>';
                        }
                    }
                },
                {
                    "searchable": false,
                    "mDataProp": "status",
                    "title": "Status",
                    "class": "text-center",
                    "render": function (data, type, row, meta) {
                        if (data == 1) {
                            return '<badge class="badge badge-success">Active</button>';
                        }else{
                            return '<badge class="badge badge-danger">Deactivated</button>';
                        }
                    }
                },
            ],
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'csv', title: 'Users List',exportOptions: { columns: ':visible'}},
            ],
        });  
        
        
        function updateUserStatus(id,type){

            var url = '{{ route('admin.users.update_status', ['user' => 'id', 'status' => 'type']) }}';
            url = url.replace('id', id).replace('type', type);

            if(type==1){
                message = "The user will be able to access the system again.";
                title = 'Activate User!';
            }else{
                message = "The user won't be able to access the system anymore.";
                title = 'Deactivate User!';
            }

            Swal.fire({
                title: 'Are you sure?',
                text: message,
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, '+title,
                closeOnConfirm: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                        }
                    });
                    $.ajax({
                        url:url,
                        method: 'GET',
                        success: function(result) {
                            if(result.status){
                                swalMessage('success','Status Update Success.',result.message);
                                users_table.ajax.url('{{route('admin.users.list')}}').load();
                            }else{
                                swalMessage('warning','Status Update Failed.',result.message);
                            }
                        }
                    }).fail(function() {
                        swalError();
                    });
                }
            })

        }

        function updateUserType(id,type){

            var url = '{{ route('admin.users.update_type', ['user' => 'id', 'status' => 'type']) }}';
            url = url.replace('id', id).replace('type', type);

            if(type==1){
                message = "The user will be able to access admin panel.";
                title = 'Assign as Administrator!';
            }else{
                message = "The user will be able to access store panel only.";
                title = 'Assign as Store Manager!';
            }

            Swal.fire({
                title: 'Are you sure?',
                text: message,
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, '+title,
                closeOnConfirm: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                        }
                    });
                    $.ajax({
                        url:url,
                        method: 'GET',
                        success: function(result) {
                            if(result.status){
                                swalMessage('success','User Type Update Success.',result.message);
                                users_table.ajax.url('{{route('admin.users.list')}}').load();
                            }else{
                                swalMessage('warning','User Type Update Failed.',result.message);
                            }
                        }
                    }).fail(function() {
                        swalError();
                    });
                }
            })

        }

        function deleteUser(id){

            var url = '{{ route('admin.users.destroy', ['user' => 'id']) }}';
            url = url.replace('id', id);

            Swal.fire({
                title: 'Are you sure?',
                text: "User and all of the user's data will be deleted.",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Delete User.',
                closeOnConfirm: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                        }
                    });
                    $.ajax({
                        url:url,
                        method: 'DELETE',
                        success: function(result) {
                            if(result.status){
                                swalMessage('success','Delete Success.',result.message);
                                users_table.ajax.url('{{route('admin.users.list')}}').load();
                            }else{
                                swalMessage('warning','Delete Failed.',result.message);
                            }
                        }
                    }).fail(function() {
                        swalError();
                    });
                }
            })

        }

    </script>

@endpush
